FROM node:latest as node
COPY . .

FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=node /dist/exercise-frontend /usr/share/nginx/html

EXPOSE 4200 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]
