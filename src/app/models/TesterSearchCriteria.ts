export interface TesterSearchCriteria {
  countries?: string[];
  devices?: number[];
}
