export interface TesterDto {
  testerId?: number;
  firstName?: string;
  lastName?: string;
  country?: string;
  lastLogin?: Date;
  experience?: number;
  deviceIds?: number[];
}
