export interface DeviceDto {
  deviceId: number;
  description: string;
}
