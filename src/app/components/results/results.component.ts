import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {TesterService} from '../../services/tester.service';
import {TesterDto} from '../../models/TesterDto';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit, OnChanges {

  @Input()
  results: TesterDto[];
  dataSource: MatTableDataSource<TesterDto>;
  displayedColumns = ['firstName', 'lastName', 'country', 'experience', 'lastLogin'];
  constructor() { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<TesterDto>(this.results);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.dataSource = new MatTableDataSource<TesterDto>(this.results);
  }

}
