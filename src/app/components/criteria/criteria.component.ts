import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TesterService} from '../../services/tester.service';
import {DeviceDto} from '../../models/DeviceDto';
import {DeviceService} from '../../services/device.service';
import {MatSelect} from '@angular/material/select';
import {MatOption} from '@angular/material/core';
import {TesterSearchCriteria} from '../../models/TesterSearchCriteria';
import {ImportService} from '../../services/import.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-criteria',
  templateUrl: './criteria.component.html',
  styleUrls: ['./criteria.component.scss']
})
export class CriteriaComponent implements OnInit {

  criteriaFormGroup: FormGroup;
  countries: string[];
  devices: DeviceDto[];
  @Output() criteria = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private testerService: TesterService,
    private deviceService: DeviceService,
    private importService: ImportService,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.testerService.getAllCountries().subscribe(data => this.countries = data);
    this.deviceService.getAllDevices().subscribe(data => this.devices = data);
    this.criteriaFormGroup = this.formBuilder.group({
      countries: [''],
      devices: ['']
    });
  }

  toggleAllCountriesSelection(event) {
    if (event.checked) {
      this.criteriaFormGroup.controls.countries.patchValue([...this.countries]);
    } else {
      this.criteriaFormGroup.controls.countries.patchValue([]);
    }
  }

  toggleAllDevicesSelection(event) {
    if (event.checked) {
      this.criteriaFormGroup.controls.devices.patchValue([...this.devices.map(item => item.deviceId)]);
    } else {
      this.criteriaFormGroup.controls.devices.patchValue([]);
    }
  }

  submitCriteria(criteria: TesterSearchCriteria) {
    this.criteria.emit(criteria);
  }

  importDataFromCsv() {
    this.importService.importData().subscribe(data => {
      if (data) {
        this.toastrService.success('Data correctly imported!');
        this.testerService.getAllCountries().subscribe(data => this.countries = data);
        this.deviceService.getAllDevices().subscribe(data => this.devices = data);
      } else {
        this.toastrService.error('Data already imported!');
      }
    });
  }
}
