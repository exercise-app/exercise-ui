import { Component } from '@angular/core';
import {TesterDto} from './models/TesterDto';
import {TesterService} from './services/tester.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  searchResults: TesterDto[] = [];
  title = 'exercise-frontend';

  constructor(private testerService: TesterService, private toastrService: ToastrService){}

  onCriteriaSubmit(event) {
    if (event.countries.length > 0 && event.devices.length > 0) {
      this.testerService.getTestersByCriteriaSortedByExperience(event.countries, event.devices).subscribe(data => {
        if (data.length > 0) {
          this.searchResults = data;
          this.toastrService.success(`There were found ${data.length} testers.`);
        } else {
          this.toastrService.error(`No testers found for criteria.`);
        }
      });
    } else {
      this.toastrService.warning(`Please provide all criteria.`);
    }
  }
}
