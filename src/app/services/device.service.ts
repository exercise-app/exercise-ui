import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DeviceDto} from '../models/DeviceDto';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  readonly deviceApi = 'http://localhost:8090/api/device';

  constructor(private http: HttpClient) {}

  getAllDevices(): Observable<DeviceDto[]> {
    return this.http.get<DeviceDto[]>(`${this.deviceApi}/all`);
  }
}
