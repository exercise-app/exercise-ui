import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TesterDto} from '../models/TesterDto';

@Injectable({
  providedIn: 'root'
})
export class TesterService {

  readonly testerApi = 'http://localhost:8090/api/tester';

  constructor(private http: HttpClient) {}

  getAllCountries(): Observable<string[]> {
    return this.http.get<string[]>(`${this.testerApi}/countries`);
  }

  getTestersByCriteriaSortedByExperience(selectedCountries: string[], selectedDevices: number[]): Observable<TesterDto[]> {
    return this.http.get<TesterDto[]>(`${this.testerApi}/all`, {
      params: {
        countries: selectedCountries,
        devices: selectedDevices.toString()
      }
    });
  }
}
