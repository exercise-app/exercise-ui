import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImportService {
  readonly importApi = 'http://localhost:8090/api/csv/import';

  constructor(private http: HttpClient) {}

  importData(): Observable<boolean> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      })
    };

    return this.http.get<boolean>(`${this.importApi}`, httpOptions);
  }
}
